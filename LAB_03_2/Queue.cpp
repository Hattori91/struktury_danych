#include <iostream>
#include <time.h>

using namespace std;

class queue
{
	
	public:

		

		struct wskaz
		{
			wskaz *next;
			int value;
		};

		wskaz *head;
		wskaz *tail;
		queue();
		~queue();
		void push(int);
		void pop();
		void display();
		void size();
		int top();


};

class queue_table
{
	private:

	int *tab;
	int pozycja;
	int max;
	int iter;

	public:
		queue_table();
		void push(int);
		void pop();
		void display();
		int top();
		void size();
		bool isEmpty();
};





queue::queue(){
	head=NULL;
}

queue::~queue(){
	delete head;
}

void queue::push(int a){

	if (head==NULL)
	{
		wskaz *nowy=new wskaz;
		head=nowy;
		head->value=a;
		tail=nowy;
		nowy->next=NULL;

	}else
	{
		wskaz *nowy = new wskaz;			//tworzymy nowy w�ze�, nowy egzemplarz struktury
		head->next=nowy;					//jako kolejny element w stosie b�dzie glowa, czyli nasz null
		nowy->value=a;						//dopisujemy do niego wartos� podana w parametrze push(int)						
		head=nowy;
		nowy->next=NULL;
	}

}

void queue::pop(){



	if (tail!=NULL)
	{
		tail=tail->next;
	}
	
	else
		cout<<"KOLEJKA PUSTA"<<endl;
	

}

void queue::display(){

	wskaz *disp;
	disp=tail;


		while(disp!=NULL)
		{
		cout << disp->value << "  ";
		disp=disp->next;
		}
		cout << "\n";


}

void queue::size(){


	wskaz *disp;
	disp=tail;
	int licz=0;


		while(disp!=NULL)
		{
		licz=licz+1;
		disp=disp->next;
		}
		cout << "Liczba elementow: " << licz << endl;

}


int queue::top(){
	cout << "Wartosc poczatkowa:  " << tail->value << "\n\n" << endl;
	return tail->value;
}



queue_table::queue_table()					//konstruktor 
{
		max=100;							// maksymalny rozmiar
		tab = new int[max];					// tworzenie tablicy zlozonej z 50 komorek
		pozycja=0;							//	index tablicy
		iter=0;								// index tablicy poczatek
	
}



void queue_table::push(int a){

	int *pom;						//Wskaznik pomocniczy

	if( pozycja == (max-1) )		//Sprawdzanie czy osiagnieto ostatnia komorke tablicy
	{
		tab[pozycja]=a;
		pozycja=pozycja+1;

		pom = tab;
		max=max*2;					//W przypadku osiagniecia , powiekszenie tablicy

		tab = new int[max];
		
		for( int i=0; i < (pozycja-iter); i++ )
		{
			tab[i]=pom[iter];
			iter++;
		}

		delete pom;				//Zwalnianie pamieci

	}
	
	else
	{
		tab[pozycja]=a;
		pozycja=pozycja+1;
	}

	
}

void queue_table::pop(){

	if( this->isEmpty() == false )
	{
		tab[iter]=0;
		iter++;
	}

}

void queue_table::display(){

	if( this->isEmpty() == false )
	{
		for( int i=iter; i<pozycja; i++ )
		{
			cout << tab[i] << " ";
		}

		cout << endl;
	}
}



void queue_table::size(){

	if( this->isEmpty() == false )
		cout << "Ilosc elementow na stosie to: " << (pozycja-iter) << endl;

}

bool queue_table::isEmpty(){
	
	if( pozycja == 0 )
	{
		cout << "STOS PUSTY!" << endl;
		return true;
	}
	else
		return false;
	

}




int queue_table::top()
{
	if( this->isEmpty() == false )
	{
		cout << "Wartosc na szczycie to:" << tab[iter] << "\n\n" << endl;
		return tab[iter];
	}
}






int main(){
	
	queue kolejka1;
	queue_table kolejka2;
	double czas;
	clock_t start, koniec; //mierzenie czasu poszczegolnych algorytmow

	int n,min,max;
	cout << "Podaj ile elementow ma sie znalezc na stosie" << endl;
	cin >> n;
	int i,p;



	cout << "Podaj zakres " << endl;
	cin >> min >> max;
	

	start=clock();

	for(i=0;i<n;i++)
		kolejka1.push((rand()% max)+ min );

	koniec=clock();

	czas=(double)(koniec-start);
	cout << "Czas dla obiektu kolejka1: " << czas << endl;


	start=clock();

	for(i=0;i<n;i++)
		kolejka2.push((rand()% max)+ min );
	
	
	koniec=clock();

	czas=(double)(koniec-start);
	cout << "Czas dla obiektu kolejka2: " << czas << endl;
	


	system("Pause");


return 0;
}