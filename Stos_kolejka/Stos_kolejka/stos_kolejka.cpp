#include <iostream>
#include <time.h>

using namespace std;


class stack
{
private:

	struct wskaz
	{
		wskaz *next;
		int value;
	};

	wskaz *head;

public:
	stack();
	~stack();
	void push(int);
	void pop();
	void display();
	int top();
	void size();
	bool isEmpty();
};




stack::stack(){
	head = NULL;							// na poczatek ustawiamy head na null, czyli stos na poczatku jest zerowy
}

stack::~stack(){
	delete head;
}

void stack::push(int a){

	wskaz *nowy = new wskaz;			//tworzymy nowy w�ze�, nowy egzemplarz struktury
	nowy->value = a;						//dopisujemy do niego wartos� podana w parametrze push(int)
	nowy->next = head;					//jako kolejny element w stosie b�dzie glowa, czyli nasz null
	head = nowy;							//ustawiamy nasz nowo stworzony w�ze� jako pierwyszy (w stosie k�adziemy na wierzch i zdejmujemy zwierzchu) 
}

void stack::pop(){

	wskaz *tym;

	if (head != NULL)						//Sprawdzamy czy wskaznik nie wskazuje na NULL
	{
		tym = head;						//Kopiowanie adresu wskaznika

		cout << "Usunieto element: " << head->value << endl;
		head = head->next;
		delete tym;						 //Zwalnianie pamieci
	}
	else
		cout << "STOS PUSTY!" << endl;

}

void stack::display(){

	wskaz *disp;
	disp = head;

	if (disp != NULL)
	{

		while (disp != NULL)
		{
			cout << disp->value << "  " << endl;
			disp = disp->next;
		}
		cout << "\n" << endl;
	}
	else
		cout << "STOS PUSTY" << endl;

}



void stack::size(){

	wskaz *disp;
	disp = head;
	int licz = 0;

	if (disp != NULL)
	{

		while (disp != NULL)
		{
			disp = disp->next;
			licz = licz + 1;
		}
		cout << "\n" << endl;
		cout << "Liczba elementow: " << licz << endl;
	}
	else
		cout << "STOS PUSTY" << endl;

}

bool stack::isEmpty(){

	if (head == NULL)
	{
		cout << "STOS PUSTY!" << endl;
		return true;
	}
	else
		return false;


}




int stack::top()
{
	cout << "Wartosc na szczycie to:" << head->value << "\n\n" << endl;
	return head->value;
}


class stack_table
{
private:

	int *tab;
	int pozycja;
	int max;
	int iter;

public:
	stack_table();
	void push(int);
	void pop();
	void display();
	int top();
	void size();
	bool isEmpty();
};


stack_table::stack_table()					//konstruktor 
{
	max = 100;							// maksymalny rozmiar
	tab = new int[max];					// tworzenie tablicy zlozonej z 50 komorek
	pozycja = 0;							//	index tablicy
	iter = 0;								// index tablicy poczatek

}



void stack_table::push(int a){

	int *pom;						//Wskaznik pomocniczy

	if (pozycja == (max - 1))		//Sprawdzanie czy osiagnieto ostatnia komorke tablicy
	{
		tab[pozycja] = a;
		pozycja = pozycja + 1;

		pom = tab;
		max = max * 2;					//W przypadku osiagniecia , powiekszenie tablicy

		tab = new int[max];

		for (int i = 0; i < (pozycja - iter); i++)
		{
			tab[i] = pom[iter];
			iter++;
		}

		delete[] pom;				//Zwalnianie pamieci

	}

	else
	{
		tab[pozycja] = a;
		pozycja = pozycja + 1;
	}

	
}

void stack_table::pop(){

	if (this->isEmpty() == false)
	{
		
		tab[pozycja] = 0;
		pozycja--;
	}

}

void stack_table::display(){

	if (this->isEmpty() == false)
	{
		for (int i = iter; i<pozycja; i++)
		{
			cout << tab[i] << " ";
		}

		cout << endl;
	}
}



void stack_table::size(){

	if (this->isEmpty() == false)
		cout << "Ilosc elementow na stosie to: " << (pozycja - iter) << endl;

}

bool stack_table::isEmpty(){

	if (pozycja == 0)
	{
		cout << "STOS PUSTY!" << endl;
		return true;
	}
	else
		return false;


}




int stack_table::top()
{
	if (this->isEmpty() == false)
	{
		cout << "Wartosc na szczycie to:" << tab[pozycja-1] << "\n\n" << endl;
		return tab[pozycja-1];
	}
}




class queue
{

public:



	struct wskaz
	{
		wskaz *next;
		int value;
	};

	wskaz *head;
	wskaz *tail;
	queue();
	~queue();
	void push(int);
	void pop();
	void display();
	void size();
	int top();


};

class queue_table
{
private:

	int *tab;
	int pozycja;
	int max;
	int iter;

public:
	queue_table();
	void push(int);
	void pop();
	void display();
	int top();
	void size();
	bool isEmpty();
};





queue::queue(){
	head = NULL;
}

queue::~queue(){
	delete head;
}

void queue::push(int a){

	if (head == NULL)
	{
		wskaz *nowy = new wskaz;
		head = nowy;
		head->value = a;
		tail = nowy;
		nowy->next = NULL;

	}
	else
	{
		wskaz *nowy = new wskaz;			//tworzymy nowy w�ze�, nowy egzemplarz struktury
		head->next = nowy;					//jako kolejny element w stosie b�dzie glowa, czyli nasz null
		nowy->value = a;						//dopisujemy do niego wartos� podana w parametrze push(int)						
		head = nowy;
		nowy->next = NULL;
	}

}

void queue::pop(){



	if (tail != NULL)
	{
		tail = tail->next;
	}

	else
		cout << "KOLEJKA PUSTA" << endl;


}

void queue::display(){

	wskaz *disp;
	disp = tail;


	while (disp != NULL)
	{
		cout << disp->value << "  ";
		disp = disp->next;
	}
	cout << "\n";


}

void queue::size(){


	wskaz *disp;
	disp = tail;
	int licz = 0;


	while (disp != NULL)
	{
		licz = licz + 1;
		disp = disp->next;
	}
	cout << "Liczba elementow: " << licz << endl;

}


int queue::top(){
	cout << "Wartosc poczatkowa:  " << tail->value << "\n\n" << endl;
	return tail->value;
}



queue_table::queue_table()					//konstruktor 
{
	max = 100;							// maksymalny rozmiar
	tab = new int[max];					// tworzenie tablicy zlozonej z 50 komorek
	pozycja = 0;							//	index tablicy
	iter = 0;								// index tablicy poczatek

}



void queue_table::push(int a){

	int *pom;						//Wskaznik pomocniczy

	if (pozycja == (max - 1))		//Sprawdzanie czy osiagnieto ostatnia komorke tablicy
	{
		tab[pozycja] = a;
		pozycja = pozycja + 1;

		pom = tab;
		max = max * 2;					//W przypadku osiagniecia , powiekszenie tablicy

		tab = new int[max];

		for (int i = 0; i < (pozycja - iter); i++)
		{
			tab[i] = pom[iter];
			iter++;
		}

		delete [] pom;				//Zwalnianie pamieci

	}

	else
	{
		tab[pozycja] = a;
		pozycja = pozycja + 1;
	}


}

void queue_table::pop(){

	if (this->isEmpty() == false)
	{
		tab[iter] = 0;
		iter++;
	}

}

void queue_table::display(){

	if (this->isEmpty() == false)
	{
		for (int i = iter; i<pozycja; i++)
		{
			cout << tab[i] << " ";
		}

		cout << endl;
	}
}



void queue_table::size(){

	if (this->isEmpty() == false)
		cout << "Ilosc elementow w kolejce to: " << (pozycja - iter) << endl;

}

bool queue_table::isEmpty(){

	if (pozycja == 0)
	{
		cout << "Kolejka pusta!" << endl;
		return true;
	}
	else
		return false;


}




int queue_table::top()
{
	if (this->isEmpty() == false)
	{
		cout << "Wartosc na szczycie to:" << tab[iter] << "\n\n" << endl;
		return tab[iter];
	}
}






int main(){

	queue kolejka1;
	queue_table kolejka2;
	stack stos1;
	stack_table stos2;


	kolejka1.push(1);
	kolejka1.push(2);
	kolejka1.push(3);

	kolejka1.top();

	kolejka2.push(1);
	kolejka2.push(2);
	kolejka2.push(3);

	kolejka2.top();

	stos1.push(1);
	stos1.push(2);
	stos1.push(3);

	stos1.top();

	stos2.push(1);
	stos2.push(2);
	stos2.push(3);

	stos2.top();

	stos2.display();


	/*double czas;
	clock_t start, koniec; //mierzenie czasu poszczegolnych algorytmow

	int n, min, max;
	cout << "Podaj ile elementow ma sie znalezc na stosie" << endl;
	cin >> n;
	int i, p;



	cout << "Podaj zakres " << endl;
	cin >> min >> max;


	start = clock();

	for (i = 0; i<n; i++)
		kolejka1.push((rand() % max) + min);

	koniec = clock();

	czas = (double)(koniec - start);
	cout << "Czas dla obiektu kolejka1: " << czas << endl;


	start = clock();

	for (i = 0; i<n; i++)
		kolejka2.push((rand() % max) + min);


	koniec = clock();

	czas = (double)(koniec - start);
	cout << "Czas dla obiektu kolejka2: " << czas << endl;

	*/

	system("Pause");


	return 0;
}